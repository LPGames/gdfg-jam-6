using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroInteractor : MonoBehaviour
{

    HeroController hero;

    private void Awake() 
    {
        hero = FindObjectOfType<HeroController>();
    }

    private void Update() 
    {
        if(hero == null)
        {
            hero = FindObjectOfType<HeroController>();
        }    
    }

    private void OnTriggerEnter2D(Collider2D other) 
    {
        print("Hit " + other.name);
        ChangeDirection();
    }
    void ChangeDirection()
    {
        hero.transform.localScale = new Vector2(-Mathf.Sign(hero.GetComponent<Rigidbody2D>().velocity.x), 1f);
        if(hero.isFacingRight)
        {
            hero.isFacingRight = false;
        }
        else
        {
            hero.isFacingRight = true;
        }
    }
}
