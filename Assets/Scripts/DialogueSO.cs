using UnityEngine;

[CreateAssetMenu()]
public class DialogueSO : ScriptableObject
{
    [TextArea(3,10)]
    public string[] sentences;
}
