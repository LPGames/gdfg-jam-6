using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using UnityEngine.UI;

public class TowerCharger : MonoBehaviour
{
    [SerializeField]
    private Slider _progressSlider;
    [SerializeField]
    private Image _progressEndImage;
    private bool _isCharging;
    
    public float duration = 1f;
    [HideInInspector] public bool isCharged;
    
    private SpriteRenderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (isCharged || _isCharging)
        {
            return;
        }
        _progressSlider.gameObject.SetActive(true);
        _progressEndImage.gameObject.SetActive(false);
        _progressSlider.value = 0;
        SetGrayscale();
        StartCoroutine(ApplyGrayscale(duration));
        _isCharging = true;
    }

    private IEnumerator ApplyGrayscale(float duration)
    {
        float time = 0;
        while (duration > time)
        {
            float ratio = time / duration;
            float progress = ratio;
            SetGrayscale(1 - progress);
            _progressSlider.value = progress;

            time += Time.deltaTime;
            yield return null;
        }   

        SetGrayscale(0);
        _progressSlider.value = 1;
        _progressEndImage.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.1f);
        _progressSlider.gameObject.SetActive(false);
        isCharged = true;
        _isCharging = false;
    }

    public void SetGrayscale(float amount = 1)
    {
        _renderer.material.SetFloat("_GrayscaleAmount", amount);
    }
}
