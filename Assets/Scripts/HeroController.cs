using System.Collections;
using Events;
using MoreMountains.Feedbacks;
using UnityEngine;

public class HeroController : MonoBehaviour
{
    [SerializeField] private float speed = 2f;
    [SerializeField] private MMFeedbacks damageFeedback;

    [HideInInspector] public Collider2D obstacle;
    [HideInInspector] public Vector2 obstacleForce;

    // Hero Components
    private Animator _anim;
    private Rigidbody2D _rb;

    // Other Vars
    private Vector3 _currentPosition;
    private bool _isFalling;
    private bool _isEntering = true;
    public bool isDead;
    private bool _animOverride;
    public bool isFacingRight = true;

    // Animator Hashes
    private static readonly int IsRunningHash = Animator.StringToHash("isRunning");
    private static readonly int IsFallingHash = Animator.StringToHash("isFalling");

    void FixedUpdate()
    {
        if (_isEntering || isDead)
        {
            return;
        }
        HandleFalling();
        HandleHeroMovement();
    }

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody2D>();
    }

    private void HandleHeroMovement()
    {
        // This is used by timeline.
        if (_animOverride)
        {
            _rb.velocity = Vector2.zero;
            return;
        }
        _anim.SetBool(IsRunningHash, true);

        if(isFacingRight)
        {
            _rb.velocity = new Vector2(speed, _rb.velocity.y);
        }
        else
        {
            _rb.velocity = new Vector2(-speed, _rb.velocity.y);
        }
    }

    private void HandleFalling()
    {
        float fallIncrement = _currentPosition.y - transform.position.y;
        if (fallIncrement > 0.01 && !_isFalling)
        {
            _isFalling = true;
            _anim.SetBool(IsFallingHash, true);
        }
        else if (fallIncrement <= 0 && _isFalling)
        {
            _isFalling = false;
            _anim.SetBool(IsFallingHash, false);
        }
        _currentPosition = transform.position;
    }

    // This is used by timeline.
    public void SetRunning(bool isRunning)
    {
        _animOverride = !isRunning;
        _anim.SetBool(IsRunningHash, isRunning);
    }

    public void AttackObstacle(float delay, bool destroy = false)
    {
        if (!obstacle)
        {
            return;
        }

        StartCoroutine(DoAttackObstacle(delay, destroy));
    }

    private IEnumerator DoAttackObstacle(float delay, bool destroy = false)
    {
        yield return new WaitForSeconds(delay);
        Rigidbody2D rb = obstacle.GetComponentInParent<Rigidbody2D>();

        if (rb)
        {
            _anim.SetTrigger("Attack");
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.AddForce(obstacleForce * 2000);
        }

        yield return new WaitForFixedUpdate();
        SetRunning(true);

        if (destroy)
        {
            Destroy(obstacle.gameObject.transform.parent.gameObject, 2);
        }

        obstacle = default;
    }

    private void IsEntering(GameObject hero)
    {
        _isEntering = true;
    }

    private void IsDoneEntering()
    {
        _isEntering = false;
    }

    private void HeroDeath()
    {
        isDead = true;
        _anim.SetTrigger("Death");
    }

    private void HandleDamage(float damage)
    {
        if (damageFeedback)
        {
            damageFeedback.PlayFeedbacks();
        }
    }

    private void OnEnable()
    {
        HeroEvents.OnHeroEntering += IsEntering;
        HeroEvents.OnHeroEntered += IsDoneEntering;
        HeroEvents.OnHeroDeath += HeroDeath;
        HeroEvents.OnHeroTakesDamage += HandleDamage;
    }
    private void OnDisable()
    {
        HeroEvents.OnHeroEntering -= IsEntering;
        HeroEvents.OnHeroEntered -= IsDoneEntering;
        HeroEvents.OnHeroDeath -= HeroDeath;
        HeroEvents.OnHeroTakesDamage -= HandleDamage;
    }
}
