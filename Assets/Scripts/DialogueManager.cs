using System.Collections;
using System.Collections.Generic;
using Events;
using TMPro;
using UnityEngine;

public class DialogueManager : MonoBehaviour
{
    private Animator _anim;
    private AudioSource _typeAudio;
    public DialogueSO Dialogue;
    public TextMeshProUGUI DialogueText;
    public TextMeshProUGUI ButtonText;
    [SerializeField]
    private float _waitTime = 5;

    private Queue<string> sentences;

    private static readonly int HideHash = Animator.StringToHash("Hide");
    private static readonly int ShowHash = Animator.StringToHash("Show");

    public void StartDialogue()
    {
        sentences.Clear();

        foreach (string sentence in Dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();

        _anim.ResetTrigger(HideHash);
        _anim.SetTrigger(ShowHash);
    }

    public void DisplayNextSentence()
    {
        StopAllCoroutines();
        if (sentences.Count == 0)
        {
            DialogueTrigger.EndDialogue();
            return;
        }
        else if (sentences.Count == 1)
        {
            ButtonText.text = "Close".ToUpper();
        }
        else
        {
            ButtonText.text = "Next".ToUpper();
            // We don't want to auto close...
            // StartCoroutine(AutoTriggerNextSentence(_waitTime));
        }

        string nextSentence = sentences.Dequeue().ToUpper();
        StartCoroutine(TypeSentence(nextSentence));
    }

    IEnumerator TypeSentence(string sentence)
    {
        DialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            _typeAudio.Stop();
            DialogueText.text += letter;
            _typeAudio.Play();
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator AutoTriggerNextSentence(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        DisplayNextSentence();
    }

    private void EndDialogue()
    {
        _anim.SetTrigger(HideHash);
    }

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _typeAudio = GetComponent<AudioSource>();
    }

    private void CheckForLevelSettings()
    {
        LevelSettings levelSettings = LevelSettings.Instance;
        if (levelSettings)
        {
            Dialogue = levelSettings.Dialogue;
        }
    }

    private void OnEnable()
    {
        DialogueTrigger.OnStartDialogue += StartDialogue;
        DialogueTrigger.OnContinueDialogue += DisplayNextSentence;
        DialogueTrigger.OnEndDialogue += EndDialogue;
        ScenesManager.OnAllScenesLoaded += CheckForLevelSettings;
        HeroEvents.OnHeroDeath += EndDialogue;
    }

    private void OnDisable()
    {
        DialogueTrigger.OnStartDialogue -= StartDialogue;
        DialogueTrigger.OnContinueDialogue -= DisplayNextSentence;
        DialogueTrigger.OnEndDialogue -= EndDialogue;
        ScenesManager.OnAllScenesLoaded -= CheckForLevelSettings;
        HeroEvents.OnHeroDeath -= EndDialogue;
    }

    void Start()
    {
        sentences = new Queue<string>();
    }

}
