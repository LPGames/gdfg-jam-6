using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
[RequireComponent(typeof(TowerPos))]
public class TowerEditor : MonoBehaviour
{
    TowerPos towerPos;

    private void Awake()
    {
        towerPos = GetComponent<TowerPos>();
    }

    void Update()
    {
        SnapToGrid();
    }


    private void SnapToGrid()
    {
        int gridSize = towerPos.GetGridSize();
        transform.position = new Vector2(
            towerPos.GetGridPos().x * gridSize,
            towerPos.GetGridPos().y * gridSize
        );
    }
}
