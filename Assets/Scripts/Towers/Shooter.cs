using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] GameObject projectile;
    [SerializeField] GameObject barrel;
    [SerializeField] GameObject endPos;

    Animator anim;
    LookAt lookAt;
    private ScenesManager _scenesManager;
    private TowerCharger _charger;

    private void Start()
    {
        anim = GetComponent<Animator>();
        lookAt = GetComponentInChildren<LookAt>();
        _charger = GetComponentInChildren<TowerCharger>();
        _scenesManager = ScenesManager.Instance;
    }

    private void Update()
    {
        if (!_charger.isCharged)
        {
            return;
        }

        if(lookAt.GetTarget() != null && CanSeeTarget())
        {
            anim.SetTrigger("Fire");
            _charger.isCharged = false;
        }
    }

    bool CanSeeTarget()
    {
        bool val = false;

        RaycastHit2D hit = Physics2D.Linecast(barrel.transform.position, endPos.transform.position, LayerMask.GetMask("Hero", "Ground"));

        if(hit.collider != null)
        {
            if(hit.collider.gameObject.CompareTag("Hero"))
            {
                val = true;
                // Dont shoot a dead hero.
                HeroController heroController = hit.collider.gameObject.GetComponent<HeroController>();
                if (heroController && heroController.isDead)
                {
                    val = false;
                }
            }
            else
            {
                val = false;
            }
            Debug.DrawLine(barrel.transform.position, hit.point, Color.red);
        }
        else
        {
            Debug.DrawLine(barrel.transform.position, endPos.transform.position, Color.blue);
        }
        return val;
    }

    public void Fire()
    {
        _scenesManager.InstantiateInScene(projectile, barrel.transform.position, Quaternion.identity);
    }

    private void InitializeColdStart()
    {
        _scenesManager = ScenesManager.Instance;
    }

    private void OnEnable()
    {
        EditorColdStartup.OnColdStartFinished += InitializeColdStart;
    }

    private void OnDisable()
    {
        EditorColdStartup.OnColdStartFinished -= InitializeColdStart;
    }
}
