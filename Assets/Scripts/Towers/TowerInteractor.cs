using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerInteractor : MonoBehaviour
{
    [SerializeField] GameObject rangeDisply = null;

    private void Start()
    {
        if (rangeDisply != null)
        {
            rangeDisply.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Player") && rangeDisply != null)
        {
            rangeDisply.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && rangeDisply != null)
        {
            rangeDisply.SetActive(false);
        }
    }
}
