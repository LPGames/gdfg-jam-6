using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerPos : MonoBehaviour
{

    Vector2Int gridPos;

    const int gridSize = 1;


    public int GetGridSize()
    {
        return gridSize;
    }

    public Vector2Int GetGridPos()
    {
        return new Vector2Int(
            Mathf.RoundToInt(transform.position.x / gridSize),
            Mathf.RoundToInt(transform.position.y / gridSize)
        );
    }
}
