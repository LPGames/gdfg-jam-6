using System;
using System.Collections.Generic;
using UnityEngine;

public class TowerFactory : MonoBehaviour
{
    public static event Action<TowerTypeSO> OnTowerPlaced;
    public static event Action<TowerTypeSO> OnTowerCollected;

    [SerializeField] int towerLimit = 2;
    [SerializeField] TowerTypeSO activeTowerType;
    private ScenesManager _scenesManager;

    private Dictionary<GameObject, TowerTypeSO> _placedTowerMap = new Dictionary<GameObject, TowerTypeSO>();
    private CircleCollider2D _debugTowerCollider;
    private Vector2 _debugTowerPosition;
    private void Start()
    {
        _scenesManager = ScenesManager.Instance;
    }

    private void SwitchTower(TowerTypeSO tower)
    {
        activeTowerType = tower;
        towerLimit = activeTowerType.currentItemCount;
    }

    public void TryPlaceTower()
    {
        Collider2D towerInWayCollider;
        if (CanSpawnTower(activeTowerType, transform.position, out towerInWayCollider))
        {
            if (towerLimit > 0)
            {
                DoPlaceTower();
            }
        }
        else if (towerInWayCollider && towerInWayCollider.gameObject.IsInLayerMasks(LayerMask.GetMask("Towers")))
        {
            DoCollectTower(towerInWayCollider);
        }
    }

    private void DoCollectTower(Collider2D towerCollider)
    {
        GameObject tower = towerCollider.gameObject;
        TowerTypeSO towerSO = _placedTowerMap[tower];
        towerSO.currentItemCount++;
        OnTowerCollected?.Invoke(towerSO);

        // Cleanup
        _placedTowerMap.Remove(tower);
        Destroy(tower);
    }

    void DoPlaceTower()
    {
        var newTower = _scenesManager.InstantiateInScene(activeTowerType.prefab.gameObject, new Vector2(transform.position.x, transform.position.y - 1), Quaternion.identity) as GameObject;
        _placedTowerMap.Add(newTower, activeTowerType);
        activeTowerType.currentItemCount--;
        OnTowerPlaced?.Invoke(activeTowerType);
    }

    bool CanSpawnTower(TowerTypeSO towerType, Vector2 pos, out Collider2D collectTower)
    {
        collectTower = null;
        CircleCollider2D towerCollider = towerType.prefab.GetComponent<CircleCollider2D>();
        _debugTowerCollider = towerCollider;
        _debugTowerPosition = pos;
        Collider2D other = Physics2D.OverlapCircle(pos + towerCollider.offset, towerCollider.radius,
            LayerMask.GetMask("Towers", "Dialogue", "Ground"));

        if (other != null)
        {
            if (other.gameObject.IsInLayerMasks(LayerMask.GetMask("Towers")))
            {
                collectTower = other;
            }

            return false;
        }
        else
        {
            return true;
        }
    }

    void OnDrawGizmos()
    {
        if (!_debugTowerCollider)
        {
            return;
        }

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(_debugTowerPosition + _debugTowerCollider.offset, _debugTowerCollider.radius);
    }

    private void InitializeColdStart()
    {
        _scenesManager = ScenesManager.Instance;
    }

    private void OnEnable()
    {
        TrapsManager.OnTrapSelected += SwitchTower;
        OnTowerPlaced += SwitchTower;
        OnTowerCollected += SwitchTower;
        EditorColdStartup.OnColdStartFinished += InitializeColdStart;
    }

    private void OnDisable()
    {
        TrapsManager.OnTrapSelected -= SwitchTower;
        OnTowerPlaced -= SwitchTower;
        OnTowerCollected -= SwitchTower;
        EditorColdStartup.OnColdStartFinished -= InitializeColdStart;
    }
}
