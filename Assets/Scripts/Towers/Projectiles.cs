using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;

public class Projectiles : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    [SerializeField] float damage = 1f;
    [SerializeField] float destroyTime = 5f;
    [SerializeField] AudioSource destroyAudio;

    LookAt lookAt;

    Transform target;

    private SpriteRenderer _renderer;
    private void Awake()
    {
        lookAt = FindObjectOfType<LookAt>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        target = lookAt.GetTarget();
        StartCoroutine(DestroyGO());
    }

    void Update()
    {
        if (target == null) return;
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        var dir = target.position - transform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Hero"))
        {
            Debug.Log("boom");
            HeroEvents.HeroTookDamage(damage);
            destroyAudio.Play();
            _renderer.enabled = false;
            Destroy(gameObject, 2f);
        }
    }

    IEnumerator DestroyGO()
    {
        yield return new WaitForSecondsRealtime(destroyTime);
        Destroy(gameObject);
    }
}
