using UnityEngine;

[CreateAssetMenu()]
public class TowerTypeSO : ScriptableObject, ISerializationCallbackReceiver
{
    public Transform prefab;

    public Sprite itemFrameImage;
    public int initialItemCount;
    [HideInInspector]
    public int currentItemCount;

    public void OnAfterDeserialize()
    {
        currentItemCount = initialItemCount;
    }

    public void OnBeforeSerialize()
    {
        // Noting to do here.
    }
}
