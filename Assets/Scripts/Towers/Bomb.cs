using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Events;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] float damage = 2f;
    Animator anim;
    private AudioSource _explodeAudio;
    private CinemachineImpulseSource _explodeImpulse;

    private void Start()
    {
        anim = GetComponent<Animator>();
        _explodeAudio = GetComponent<AudioSource>();
        _explodeImpulse = GetComponent<CinemachineImpulseSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.CompareTag("Hero"))
        {
            anim.SetTrigger("startFuse");
        }
    }

    public void DealDamage()
    {
        if (_explodeAudio)
        {
            _explodeAudio.Play();
        }

        if (_explodeImpulse)
        {
            _explodeImpulse.GenerateImpulse();
        }
        // Triggered by animation event.
        HeroEvents.HeroTookDamage(damage);
    }

    public void DestroyBomb()
    {
        // Triggered by animation event.
        Destroy(gameObject);
    }
}
