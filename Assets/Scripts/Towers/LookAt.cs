using UnityEngine;

public class LookAt : MonoBehaviour
{
    Transform target = null;
    private TowerCharger _charger;
    private SpriteRenderer _renderer;

    // In case we change our mind...
    private bool _onlyLookWhenCharged;

    private void Awake()
    {
        _charger = GetComponent<TowerCharger>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (_onlyLookWhenCharged && !_charger.isCharged)
        {
            return;
        }
        SetTarget();
        if (target == null) return;
        var angle = Mathf.Atan2(FacingDirection.y, FacingDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        if (FacingLeft && !SpriteLookingLeft)
        {
            _renderer.flipY = true;
        }
        else if (!FacingLeft && SpriteLookingLeft)
        {
            _renderer.flipY = false;
        }
    }

    void SetTarget()
    {
        var sceneEnemies = FindObjectsOfType<HeroController>();
        if (sceneEnemies.Length == 0) return;

        Transform closestEnemy = sceneEnemies[0].transform;

        foreach (HeroController testEnemy in sceneEnemies)
        {
            closestEnemy = GetClosest(closestEnemy, testEnemy.transform);
        }

        target = closestEnemy;
    }

    public Transform GetClosest(Transform transformA, Transform transformB)
    {
        var distToA = Vector2.Distance(transform.position, transformA.position);
        var distToB = Vector2.Distance(transform.position, transformB.position);

        if (distToA < distToB)
        {
            return transformA;
        }

        return transformB;
    }

    public Transform GetTarget()
    {
        return target;
    }

    // Helper variables for more readable code.
    private bool FacingLeft => FacingDirection.x < 0;
    private bool SpriteLookingLeft => _renderer.flipY;
    private Vector3 FacingDirection => target ? target.position - transform.position : Vector3.left;
}
