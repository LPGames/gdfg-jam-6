using System.Diagnostics;
using Events;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    private PlayerInput _playerInput;
    private const string GameSceneName = "GameLevels";
    private const string TutorialSceneName = "Tutorials";
    private const string MenuSceneName = "Menu";
    [SerializeField] private GameObject pauseMenu;
    [SerializeField] private GameObject winMenu;
    [SerializeField] private GameObject loseMenu;

    [SerializeField] private GameObject onScreenControls;

    private ScenesManager _scenesManager;

    private bool _isShowing;
    private bool _winShown;
    private bool _isLoading;

    public void StartGame()
    {
        if (_isLoading)
        {
            return;
        }

        _isLoading = true;
        SceneManager.LoadSceneAsync(GameSceneName);
    }

    public void StartTutorial()
    {
        if (_isLoading)
        {
            return;
        }

        _isLoading = true;
        SceneManager.LoadSceneAsync(TutorialSceneName);
    }

    public void RestartCurrentLevel()
    {
        _scenesManager.ReloadCurrentScene();
    }

    public void StartNextLevel()
    {
        _scenesManager.LoadNextScene();
    }

    public void QuitGame()
    {
        if (_isLoading)
        {
            return;
        }

        _isLoading = true;
        Time.timeScale = 1;
        SceneManager.LoadScene(MenuSceneName);
    }

    public void OpenSteamLink()
    {
        Application.OpenURL("https://store.steampowered.com/app/1769870/Tower_Offender/");
    }

    private void CloseMenu(GameObject menu)
    {
        if (menu)
        {
            menu.SetActive(false);
            Time.timeScale = 1;
            _isShowing = false;
        }
    }

    private void ShowMenu(GameObject menu)
    {
        if (menu)
        {
            menu.SetActive(true);
            Time.timeScale = 0;
            _isShowing = true;
        }
    }

    public void ClosePauseMenu()
    {
       CloseMenu(pauseMenu);
    }

    public void ShowPauseMenu()
    {
        ShowMenu(pauseMenu);
    }

    public void CloseWinMenu()
    {
        CloseMenu(winMenu);
    }

    public void ShowWinMenu()
    {
        ShowMenu(winMenu);
    }

    public void CloseLoseMenu()
    {
        CloseMenu(loseMenu);
    }

    public void ShowLoseMenu()
    {
        ShowMenu(loseMenu);
    }

    private void Awake()
    {
        _playerInput = new PlayerInput();
        _playerInput.Player.Menu.started += OnMenu;
        _scenesManager = ScenesManager.Instance;
        ShowOnScreenControls();
    }

    private void OnMenu(InputAction.CallbackContext context)
    {
        if (!pauseMenu)
        {
            return;
        }

        if (_isShowing)
        {
            ClosePauseMenu();
        }
        else
        {
            ShowPauseMenu();
        }
    }


    private void ShowOnScreenControls()
    {
        DoShowOnScreenControls();
    }

    private void HideOnScreenControls()
    {
        DoHideOnScreenControls();
    }

    [Conditional("UNITY_ANDROID")]
    private void DoShowOnScreenControls()
    {
        // Only enable on-screen controls for certain targets;
        onScreenControls.SetActive(true);
    }

    [Conditional("UNITY_ANDROID")]
    private void DoHideOnScreenControls()
    {
        // Only enable on-screen controls for certain targets;
        onScreenControls.SetActive(false);
    }

    private void OnEnable()
    {
        _playerInput.Player.Enable();
        DialogueTrigger.OnStartDialogue += HideOnScreenControls;
        DialogueTrigger.OnEndDialogue += ShowOnScreenControls;
        GateTrigger.OnHeroWins += ShowLoseMenu;
        HeroEvents.OnHeroDeath += ShowWinMenu;
    }

    private void OnDisable()
    {
        _playerInput.Player.Disable();
        DialogueTrigger.OnStartDialogue -= HideOnScreenControls;
        DialogueTrigger.OnEndDialogue -= ShowOnScreenControls;
        GateTrigger.OnHeroWins -= ShowLoseMenu;
        HeroEvents.OnHeroDeath -= ShowWinMenu;
    }
}
