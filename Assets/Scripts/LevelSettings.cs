using System;
using Events;
using UnityEngine;

public class LevelSettings : MonoBehaviour
{
   public static event Action OnStartTimer;

   [Header("Timer")]
   public int heroTimer;
   public bool startTimer = true;
   public bool startTimerOnPlace;
   public bool startTimerOnEndDialogue;

   [Header("Towers")]
   public TowerTypeSO[] Traps;
   public int[] TrapQuantities;

   [Header("Dialogue")]
   public DialogueSO Dialogue;

   private static LevelSettings _instance;
   public static LevelSettings Instance { get { return _instance; } }

   private bool _timerStarted;

   private void Awake()
   {
      if (_instance != null && _instance != this)
      {
         Destroy(gameObject);
      }
      else
      {
         _instance = this;
      }
   }

   public TowerTypeSO[] GetTraps()
   {
      for (int i = 0; i < Traps.Length; i++)
      {
         TowerTypeSO trap = Traps[i];
         bool trapHasQuantity = TrapQuantities.Length > i && TrapQuantities[i] != null;
         trap.initialItemCount = trapHasQuantity ? TrapQuantities[i] : 0;
      }

      return Traps;
   }

   public void StartTimer()
   {
      _timerStarted = true;
      OnStartTimer?.Invoke();
   }

   private void StartTimerOnPlace(TowerTypeSO _)
   {
      if (!_timerStarted && startTimerOnPlace)
      {
         StartTimer();
      }
   }

   private void StartTimerOnEndDialogue()
   {
      if (!_timerStarted && startTimerOnEndDialogue)
      {
         StartTimer();
      }
   }

   private void OnEnable()
   {
      TowerFactory.OnTowerPlaced += StartTimerOnPlace;
      DialogueTrigger.OnEndDialogue += StartTimerOnEndDialogue;
   }

   private void OnDisable()
   {
      TowerFactory.OnTowerPlaced -= StartTimerOnPlace;
      DialogueTrigger.OnEndDialogue -= StartTimerOnEndDialogue;
   }
}
