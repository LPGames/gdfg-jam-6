using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemFrameUI : MonoBehaviour
{
    [SerializeField]
    private Image itemFrameImage;
    [SerializeField]
    private Image itemFrameCountWrapper;
    [SerializeField]
    private TextMeshProUGUI itemFrameCount;
    [SerializeField]
    private Button[] itemFrameArrowButtons;

    public Color allowedColour = Color.blue;
    public Color disallowedColour = Color.red;

    private TrapsManager _trapsManager;

    private bool _arrowsChecked;

    private void Start()
    {
        _trapsManager = TrapsManager.Instance;
    }

    private void UpdateTrapDisplay(TowerTypeSO trap)
    {
        if (trap.currentItemCount > 0)
        {
            itemFrameCountWrapper.color = allowedColour;
        }
        else
        {
            itemFrameCountWrapper.color = disallowedColour;
        }

        if (!_arrowsChecked)
        {
            UpdateArrows();
            _arrowsChecked = true;
        }

        itemFrameImage.sprite = trap.itemFrameImage;
        itemFrameCount.text = trap.currentItemCount.ToString();
    }

    private void UpdateArrows()
    {
        if (!_trapsManager.HasMultipleTraps())
        {
            UpdateArrowButtons();
        }
        else
        {
            UpdateArrowButtons(true);
        }
    }

    private void UpdateArrowButtons(bool interactable = false)
    {
        foreach (var itemFrameArrowButton in itemFrameArrowButtons)
        {
            itemFrameArrowButton.interactable = interactable;
        }
    }

    public void NextItem()
    {
        if (!_trapsManager.HasMultipleTraps())
        {
            return;
        }
        _trapsManager.NextTrap();
    }

    public void PreviousItem()
    {
        if (!_trapsManager.HasMultipleTraps())
        {
            return;
        }
        _trapsManager.PreviousTrap();
    }

    private void ResetUI()
    {
        UpdateArrows();
    }

    private void OnEnable()
    {
        TrapsManager.OnTrapSelected += UpdateTrapDisplay;
        TowerFactory.OnTowerPlaced += UpdateTrapDisplay;
        TowerFactory.OnTowerCollected += UpdateTrapDisplay;
        ScenesManager.OnAllScenesLoaded += ResetUI;
    }

    private void OnDisable()
    {
        TrapsManager.OnTrapSelected -= UpdateTrapDisplay;
        TowerFactory.OnTowerPlaced -= UpdateTrapDisplay;
        TowerFactory.OnTowerCollected -= UpdateTrapDisplay;
        ScenesManager.OnAllScenesLoaded -= ResetUI;
    }
}
