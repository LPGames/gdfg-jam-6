using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;

public class ScenesManager : MonoBehaviour
{
    private static ScenesManager _instance;
    public static ScenesManager Instance { get { return _instance; } }

    private const string GameSceneName = "GameLevels";
    private const string TutorialSceneName = "Tutorials";
    private const string TutorialEndSceneName = "0-4-End";
    private const string GameEndSceneName = "1-99-End";
    private const string MenuSceneName = "Menu";

    public static event Action OnAllScenesLoaded;

    private Queue<string> _tutorialScenes = new Queue<string>();

    [SerializeField]
    private string[] _sceneSelection;

    [SerializeField]
    private string[] _componentScenes;

    [SerializeField]
    private Image _loadingScreen;

    [SerializeField]
    private AudioSource _levelMusic;

    [SerializeField]
    private AudioSource _gameOverMusic;

    private string _currentScene;
    private List<AsyncOperation> _loadingOperations = new List<AsyncOperation>();
    private bool _sceneIsLoading;
    private List<Object> _objectsToDestroy = new List<Object>();

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    void Start()
    {
        PlayLevelMusic();

        foreach (string tutorialScene in _sceneSelection)
        {
            _tutorialScenes.Enqueue(tutorialScene);
        }

        foreach (string componentScene in _componentScenes)
        {
            _loadingOperations.Add(SceneManager.LoadSceneAsync(componentScene, LoadSceneMode.Additive));
            _sceneIsLoading = true;
        }

        LoadNextScene();
    }

    public void LoadNextScene()
    {
        if (_tutorialScenes.Count == 0)
        {
            string currentSceneName = SceneManager.GetActiveScene().name;
            if (currentSceneName == TutorialSceneName)
            {
                SceneManager.LoadScene(TutorialEndSceneName);
            }
            else if (currentSceneName == GameSceneName)
            {
                SceneManager.LoadScene(GameEndSceneName);
            }
            return;
        }

        if (_currentScene != null)
        {
            SceneManager.UnloadSceneAsync(_currentScene);
            foreach (Object instance in _objectsToDestroy)
            {
                Destroy(instance);
            }
        }
        _currentScene = _tutorialScenes.Dequeue();
        _sceneIsLoading = true;
        ShowLoadingScreen();
        _loadingOperations.Add(SceneManager.LoadSceneAsync(_currentScene, LoadSceneMode.Additive));
    }


    void Update()
    {
        if (!_sceneIsLoading)
        {
            return;
        }

        if (_loadingOperations.Count == 0)
        {
            return;
        }

        foreach (var loadingOperation in _loadingOperations)
        {
            if (!loadingOperation.isDone)
            {
                return;
            }
        }

        // @todo We need to clear out _loadingOperations at some point or it will become HUGE!

        _sceneIsLoading = false;
        OnAllScenesLoaded?.Invoke();
        StartCoroutine(FadeLoadingScreen(2));
    }

    private void ShowLoadingScreen()
    {
        _loadingScreen.color = new Color(0,0,0,1);
        _loadingScreen.gameObject.SetActive(true);
    }

    IEnumerator FadeLoadingScreen(float duration, float targetAlpha = 0, bool active = false)
    {
        var color = _loadingScreen.color;
        float startValue = color.a;
        float time = 0;

        while (time < duration)
        {
            color.a = Mathf.Lerp(startValue, targetAlpha, time / duration);
            _loadingScreen.color = color;
            time += Time.deltaTime;
            yield return null;
        }

        color.a = targetAlpha;
        _loadingScreen.color = color;
        _loadingScreen.gameObject.SetActive(active);
    }

    public void ReloadCurrentScene()
    {
        if (_currentScene != null)
        {
            SceneManager.UnloadSceneAsync(_currentScene);
            foreach (Object instance in _objectsToDestroy)
            {
                Destroy(instance);
            }
        }

        PlayLevelMusic();

        _sceneIsLoading = true;
        ShowLoadingScreen();
        _loadingOperations.Add(SceneManager.LoadSceneAsync(_currentScene, LoadSceneMode.Additive));
    }

    public Object InstantiateInScene(Object original, Vector3 position, Quaternion rotation)
    {
        Object instance = Instantiate(original, position, rotation);
        _objectsToDestroy.Add(instance);
        return instance;
    }

    private void PlayLevelMusic()
    {
        if (_levelMusic && !_levelMusic.isPlaying)
        {
            _levelMusic.Play();
        }

        if (_gameOverMusic && _gameOverMusic.isPlaying)
        {
            _gameOverMusic.Stop();
        }
    }

    private void PlayLoseMusic()
    {
        if (_levelMusic && _levelMusic.isPlaying)
        {
            _levelMusic.Stop();
        }

        if (_gameOverMusic && !_gameOverMusic.isPlaying)
        {
            _gameOverMusic.Play();
        }
    }

    private void OnEnable()
    {
        GateTrigger.OnHeroWins += PlayLoseMusic;
    }

    private void OnDisable()
    {
        GateTrigger.OnHeroWins -= PlayLoseMusic;
    }
}
