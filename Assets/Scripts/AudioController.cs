using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioController : MonoBehaviour
{
    [SerializeField] UnityEvent onRunning;
    [SerializeField] UnityEvent onLanding;
    [SerializeField] UnityEvent onFire;
    [SerializeField] UnityEvent onDamage;

    public void PlayRunningSFX()
    {
        onRunning.Invoke();
    }

    public void PlayLandingSFX()
    {
        onLanding.Invoke();
    }

    public void PlayFireSFX()
    {
        onFire.Invoke();
    }

    public void PlayDamageSFX()
    {
        onDamage.Invoke();
    }
}
