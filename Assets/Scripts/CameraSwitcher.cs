using Cinemachine;
using Events;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraSwitcher : MonoBehaviour
{
    [SerializeField] private InputAction action;
    [SerializeField] private CinemachineVirtualCamera heroCam;
    [SerializeField] private CinemachineVirtualCamera playerCam;
    [SerializeField] private CinemachineVirtualCamera doorCam;
    [SerializeField] private CinemachineTargetGroup group;

    private float cameraWidthInWorldUnits = 17f;
    private float cameraHeightInWorldUnits = 8f;

    private Animator _anim;
    private Transform _playerTransform;
    private Transform _heroTransform;
    private bool _isPlayerInHeroRange;

    private bool _isFocussed;
    private bool _isHeroPresent;
    private static readonly int HeroCamHash = Animator.StringToHash("HeroCamState");
    private static readonly int PlayerCamHash = Animator.StringToHash("PlayerCamState");
    private static readonly int GroupCamHash = Animator.StringToHash("GroupCamState");
    private static readonly int DoorCamHash = Animator.StringToHash("DoorCamState");

    private void ResetCamera()
    {
        if (_isFocussed)
        {
            return;
        }

        if (UseGroupCam)
        {
            SwitchTo(GroupCamHash);
        }
        else
        {
            SwitchTo(PlayerCamHash);
        }
    }

    private void FocusToHero()
    {
        _isFocussed = true;
        DoFocusToHero();
    }

    private void DoFocusToHero()
    {
        if (_isHeroPresent)
        {
            SwitchTo(HeroCamHash);
        }
        else
        {
            SwitchTo(DoorCamHash);
        }
    }

    private void UnfocusToPlayer()
    {
        _isFocussed = false;
        ResetCamera();
    }

    private void SwitchTo(int cameraState)
    {
        _anim.Play(cameraState);
    }

    private void FollowHero(GameObject hero)
    {
        _heroTransform = hero.transform;
        heroCam.Follow = _heroTransform;
        _isHeroPresent = true;
        group.AddMember(_heroTransform, 1, 1);
    }


    private void UnfollowHero()
    {
        heroCam.Follow = null;
        _isHeroPresent = false;
        group.RemoveMember(_heroTransform);
        _heroTransform = null;
    }


    private void FollowPlayer(GameObject player)
    {
        _playerTransform = player.transform;
        playerCam.Follow = _playerTransform;
        group.AddMember(player.transform, 1, 1);
    }

    private void FollowDoor(GameObject door)
    {
        doorCam.Follow = door.transform;
    }

    private void Start()
    {
        action.performed += _ => FocusToHero();
        action.canceled += _ => UnfocusToPlayer();
        _isPlayerInHeroRange = UseGroupCam;
    }

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    private void Update()
    {
        if (!_heroTransform || !_playerTransform)
        {
            return;
        }

        if (_isPlayerInHeroRange != UseGroupCam)
        {
            ResetCamera();
            _isPlayerInHeroRange = UseGroupCam;
        }
    }

    private void OnEnable()
    {
        action.Enable();
        DoorEvents.OnDoorSpawned += FollowDoor;
        DoorEvents.OnDoorOpen += FocusToHero;
        DoorEvents.OnDoorClose += UnfocusToPlayer;
        HeroEvents.OnHeroEntering += FollowHero;
        ScenesManager.OnAllScenesLoaded += UnfollowHero;
        PlayerController.OnPlayerAwake += FollowPlayer;
    }

    private void OnDisable()
    {
        action.Disable();
        DoorEvents.OnDoorSpawned -= FollowDoor;
        DoorEvents.OnDoorOpen -= FocusToHero;
        DoorEvents.OnDoorClose -= UnfocusToPlayer;
        HeroEvents.OnHeroEntering -= FollowHero;
        ScenesManager.OnAllScenesLoaded -= UnfollowHero;
        PlayerController.OnPlayerAwake -= FollowPlayer;
    }

    private float HorizontalDistanceBetweenHeroAndPlayer => Mathf.Abs(_heroTransform.position.x - _playerTransform.position.x);
    private float VerticalDistanceBetweenHeroAndPlayer => Mathf.Abs(_heroTransform.position.y - _playerTransform.position.y);
    private bool UseGroupCam =>
        _isHeroPresent &&
        HorizontalDistanceBetweenHeroAndPlayer < cameraWidthInWorldUnits &&
        VerticalDistanceBetweenHeroAndPlayer < cameraHeightInWorldUnits;
}
