using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateTrigger : MonoBehaviour
{
    public static event Action OnHeroWins;
    private void OnTriggerEnter2D(Collider2D other)
    {
        OnHeroWins?.Invoke();
    }
}
