using System;
using UnityEngine;

public class TrapsManager : MonoBehaviour
{
    private static TrapsManager _instance;
    public static TrapsManager Instance { get { return _instance; } }

    public static event Action<TowerTypeSO> OnTrapSelected;

    public TowerTypeSO[] Traps;

    private int _currentTrapIndex;

    private void SetCurrentTrap(int index)
    {
        _currentTrapIndex = (index + Traps.Length) % Traps.Length;
        OnTrapSelected?.Invoke(Traps[_currentTrapIndex]);
    }

    public void NextTrap()
    {
        SetCurrentTrap(_currentTrapIndex + 1);
    }

    public void PreviousTrap()
    {
        SetCurrentTrap(_currentTrapIndex - 1);
    }

    public bool HasMultipleTraps()
    {
        return Traps.Length > 1;
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void InitializeTraps()
    {
        if (!LevelSettings.Instance)
        {
            return;
        }

        Traps = LevelSettings.Instance.GetTraps();
        if (Traps.Length == 0)
        {
            return;
        }

        foreach (var trap in Traps)
        {
            trap.currentItemCount = trap.initialItemCount;
        }

        SetCurrentTrap(0);
    }

    private void Start()
    {
        InitializeTraps();
    }

    private void OnEnable()
    {
        ScenesManager.OnAllScenesLoaded += InitializeTraps;
    }

    private void OnDisable()
    {
        ScenesManager.OnAllScenesLoaded -= InitializeTraps;
    }
}
