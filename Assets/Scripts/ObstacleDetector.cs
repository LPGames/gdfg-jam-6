using System.Collections;
using UnityEngine;

public class ObstacleDetector : MonoBehaviour
{
    [SerializeField] private HeroController heroController;
    [SerializeField] private Vector2 forceDirection;
    public float obstacleDelay = 0.75f;
    public float playerDelay = 0f;

    private string[] _attackableLayers = {"Obstacle", "Player"};
    private int LayersToAttack => LayerMask.GetMask(_attackableLayers);

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.IsInLayerMasks(LayersToAttack))
        {
            return;
        }

        if (heroController.obstacle != default)
        {
            return;
        }

        heroController.SetRunning(false);
        heroController.obstacle = other;
        heroController.obstacleForce = forceDirection;

        if (other.gameObject.IsInLayerMasks(LayerMask.GetMask("Player")))
        {
            heroController.AttackObstacle(playerDelay);
        }
        else
        {
            heroController.AttackObstacle(obstacleDelay, true);
        }
    }
}

public static class LayerMaskExtensions
{
    public static bool IsInLayerMasks(this GameObject gameObject, int layerMasks)
    {
        return (layerMasks == (layerMasks | (1 << gameObject.layer)));
    }
}
