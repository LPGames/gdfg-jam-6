using System;
using UnityEngine;

namespace Events
{
    public class DoorEvents : MonoBehaviour
    {
        public static event Action<GameObject> OnDoorSpawned;
        public static event Action OnDoorOpen;
        public static event Action OnDoorOpened;
        public static event Action OnDoorClose;

        [SerializeField]
        private GameObject _hero;

        public Vector2 heroOffset = new Vector2(0.25f, -0.36f);

        private Animator _anim;
        private static readonly int OpenHash = Animator.StringToHash("Open");
        private static readonly int CloseHash = Animator.StringToHash("Close");

        public void DoorIsOpening()
        {
            OnDoorOpen?.Invoke();
        }

        public void DoorHasOpened()
        {
            OnDoorOpened?.Invoke();
            SpawnHero();
        }

        public void DoorIsClosed()
        {
            OnDoorClose?.Invoke();
        }

        private void SpawnHero()
        {
            ScenesManager manager = ScenesManager.Instance;
            Vector3 position = transform.position + (Vector3) heroOffset;
            manager.InstantiateInScene(_hero, position, Quaternion.identity);
        }

        private void OpenDoor()
        {
            _anim.SetTrigger(OpenHash);
        }

        private void CloseDoor()
        {
            _anim.SetTrigger(CloseHash);
        }

        private void Awake()
        {
            _anim = GetComponent<Animator>();
        }

        private void Start()
        {
            OnDoorSpawned?.Invoke(gameObject);
        }

        private void InitializeColdStart()
        {
            OnDoorSpawned?.Invoke(gameObject);
        }

        private void OnEnable()
        {
            Timer.OnTimeOut += OpenDoor;
            HeroEvents.OnHeroEntered += CloseDoor;
            EditorColdStartup.OnColdStartFinished += InitializeColdStart;
        }

        private void OnDisable()
        {
            Timer.OnTimeOut -= OpenDoor;
            HeroEvents.OnHeroEntered -= CloseDoor;
            EditorColdStartup.OnColdStartFinished -= InitializeColdStart;
        }
    }
}
