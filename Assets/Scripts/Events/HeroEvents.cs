using System;
using UnityEngine;

namespace Events
{
    public class HeroEvents : MonoBehaviour
    {
        public static event Action<GameObject> OnHeroEntering;
        public static event Action OnHeroEntered;
        public static event Action<float> OnHeroTakesDamage;
        public static event Action OnHeroDeath;

        public void HeroIsEntering()
        {
            OnHeroEntering?.Invoke(gameObject);
        }

        public void HeroHasEntered()
        {
            OnHeroEntered?.Invoke();
        }

        public static void HeroTookDamage(float damage)
        {
            OnHeroTakesDamage?.Invoke(damage);
        }

        public static void HeroDies()
        {
            OnHeroDeath?.Invoke();
        }
    }
}
