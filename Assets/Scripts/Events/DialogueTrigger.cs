using System;
using UnityEngine;

namespace Events
{
    public class DialogueTrigger : MonoBehaviour
    {
        public static event Action OnStartDialogue;
        public static event Action OnContinueDialogue;
        public static event Action OnEndDialogue;
        public bool HasStarted;

        private void OnTriggerEnter2D(Collider2D other)
        {
            PlayerController controller = other.GetComponent<PlayerController>();
            if (controller)
            {
                controller.CanTalk = true;
                controller.DialogueTrigger = this;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            PlayerController controller = other.GetComponent<PlayerController>();
            if (controller)
            {
                controller.CanTalk = false;
                controller.DialogueTrigger = default;
            }

            // Only trigger ended if the dialogue actually started, not just walking through triggers.
            if (HasStarted)
            {
                EndDialogue();
            }
            HasStarted = false;
        }

        public void TriggerDialogue()
        {
            OnStartDialogue?.Invoke();
            HasStarted = true;
        }

        public void ContinueDialogue()
        {
            OnContinueDialogue?.Invoke();
        }

        public static void EndDialogue()
        {
            OnEndDialogue?.Invoke();
        }
    }
}
