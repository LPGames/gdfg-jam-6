using System;
using Events;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public static event Action<GameObject> OnPlayerAwake;

    [SerializeField]
    private float force = 200f;
    [SerializeField]
    private float fallMultiplier = 2.5f;
    [SerializeField]
    private float lowJumpMultiplier = 2f;
    [SerializeField]
    private float playerSpeed = 10f;
    [SerializeField]
    private Transform groundChecker;
    public DialogueTrigger DialogueTrigger;

    // Player Components
    private Animator _anim;
    private Rigidbody2D _rb;
    private SpriteRenderer _renderer;

    // Other Vars
    private PlayerInput _playerInput;
    private Vector3 _currentPosition;
    private Vector3 _currentMovement;
    public bool CanTalk;
    private bool _hasMovementInput;
    private bool _isFalling;
    private bool _isGrounded;
    private bool _animOverride;
    private LayerMask _groundLayer;

    // Animator Hashes
    private static readonly int JumpHash = Animator.StringToHash("Jump");
    private static readonly int IsRunningHash = Animator.StringToHash("isRunning");
    private static readonly int IsFallingHash = Animator.StringToHash("isFalling");

    void FixedUpdate()
    {
        HandleJumping();
        HandleFalling();

        if (!_isFalling)
        {
            HandleRunningAnimation();
        }

        if (_hasMovementInput)
        {
            HandlePlayerDirection();
            HandlePlayerMovement();
        }

        CheckIfGrounded();
    }

    private void OnJump(InputAction.CallbackContext context)
    {
        if (CanTalk)
        {
            if (!DialogueTrigger.HasStarted)
            {
                DialogueTrigger.TriggerDialogue();
            }
            else
            {
                DialogueTrigger.ContinueDialogue();
            }
            return;
        }

        if (!_isGrounded)
        {
            return;
        }
        _rb.velocity = new Vector2(_rb.velocity.x, force);
        _anim.SetTrigger(JumpHash);
    }

    private void OnMove(InputAction.CallbackContext context)
    {
        Vector2 currentMovementInput = context.ReadValue<Vector2>();
        _currentMovement.x = currentMovementInput.x;
        _hasMovementInput = currentMovementInput.x != 0;
    }

    void OnPlaceTower(InputAction.CallbackContext context)
    {
        if (_isGrounded)
        {
            GetComponent<TowerFactory>().TryPlaceTower();
        }
    }

    private void Awake()
    {
        _playerInput = new PlayerInput();
        _playerInput.Player.Jump.started += OnJump;
        _playerInput.Player.PlaceTower.started += OnPlaceTower;
        _playerInput.Player.Move.started += OnMove;
        _playerInput.Player.Move.canceled += OnMove;
        _playerInput.Player.Move.performed += OnMove;

        _anim = GetComponent<Animator>();
        _rb = GetComponent<Rigidbody2D>();
        _renderer = GetComponent<SpriteRenderer>();

        _groundLayer = LayerMask.GetMask("Ground", "Platforms");

        OnPlayerAwake?.Invoke(gameObject);
    }

    private void HandleJumping()
    {
        if (_rb.velocity.y < 0)
        {
            _rb.velocity += Vector2.up * Physics2D.gravity * (fallMultiplier - 1) * Time.fixedDeltaTime;
        }
        else if (_rb.velocity.y > 0)
        {
            _rb.velocity += Vector2.up * Physics2D.gravity * (lowJumpMultiplier - 1) * Time.fixedDeltaTime;
        }
    }

    private void CheckIfGrounded()
    {
        Collider2D groundCollider = Physics2D.OverlapCircle(groundChecker.position, 0.05f, _groundLayer);
        if (groundCollider != null)
        {
            _isGrounded = true;
        }
        else
        {
            _isGrounded = false;
        }
    }

    private void HandlePlayerMovement()
    {
        _rb.velocity = new Vector2(_currentMovement.x * playerSpeed, _rb.velocity.y);
    }

    private void HandleRunningAnimation()
    {
        // This is used by timeline.
        if (_animOverride)
        {
            return;
        }
        if (!_hasMovementInput && _anim.GetBool(IsRunningHash))
        {
            _anim.SetBool(IsRunningHash, false);
        }
        else if (_hasMovementInput && !_anim.GetBool(IsRunningHash))
        {
            _anim.SetBool(IsRunningHash, true);
        }
    }

    private void HandlePlayerDirection()
    {
        bool isFlipped = _renderer.flipX;
        if (isFlipped && _rb.velocity.x < 0)
        {
            _renderer.flipX = false;
        }
        else if (!isFlipped && _rb.velocity.x > 0)
        {
            _renderer.flipX = true;
        }
    }

    private void HandleFalling()
    {
        float fallIncrement = _currentPosition.y - transform.position.y;
        if (fallIncrement > 0.01 && !_isFalling)
        {
            _isFalling = true;
            _anim.SetBool(IsFallingHash, true);
        }
        else if (fallIncrement <= 0 && _isFalling)
        {
            _isFalling = false;
            _anim.SetBool(IsFallingHash, false);
        }
        _currentPosition = transform.position;
    }

    // This is used by timeline.
    public void SetRunning(bool isRunning)
    {
        _animOverride = isRunning;
        _anim.SetBool(IsRunningHash, isRunning);
    }

    private void InitializeColdStart()
    {
        OnPlayerAwake?.Invoke(gameObject);
    }

    private void OnEnable()
    {
        _playerInput.Player.Enable();
        EditorColdStartup.OnColdStartFinished += InitializeColdStart;
    }

    private void OnDisable()
    {
        _playerInput.Player.Disable();
        EditorColdStartup.OnColdStartFinished -= InitializeColdStart;
    }
}
