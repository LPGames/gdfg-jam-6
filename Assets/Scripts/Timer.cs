using System;
using Events;
using MoreMountains.Feedbacks;
using TMPro;
using UnityEngine;

/**
 * Reference material.
 * @see https://gamedevbeginner.com/how-to-make-countdown-timer-in-unity-minutes-seconds/
 */

public class Timer : MonoBehaviour
{
    public static event Action OnTimeOut;

    [SerializeField]
    private MMFeedbacks _hideFeedbacks;
    [SerializeField]
    private MMFeedbacks _showFeedbacks;

    public float numberOfSeconds = 15;
    private float _secondsRemaining = 0;
    private TextMeshProUGUI _timeText;
    private MMFeedbacks _timeFeedbacks;
    private bool _hasStarted;
    private bool _isFinished;
    private bool _isFlashing;

    private void Start()
    {
        _timeText = GetComponent<TextMeshProUGUI>();
        _timeFeedbacks = GetComponent<MMFeedbacks>();
    }

    void Update()
    {
        if (_isFinished || !_hasStarted)
        {
            return;
        }

        if (_secondsRemaining > 0)
        {
            _secondsRemaining -= Time.deltaTime;
        }
        else
        {
            _secondsRemaining = 0;
            _isFinished = true;
            OnTimeOut?.Invoke();
        }
        DisplayTime(_secondsRemaining);

        if (_secondsRemaining <= 6)
        {
            float offset = _secondsRemaining - Mathf.FloorToInt(_secondsRemaining);
            if (!_isFlashing && offset < 0.1)
            {
                _timeFeedbacks.PlayFeedbacks();
                _isFlashing = true;
            }
            else if (_isFlashing && offset > 0.9)
            {
                _isFlashing = false;
            }
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        float milliseconds = Mathf.FloorToInt(timeToDisplay * 100 % 100);
        _timeText.text = string.Format("{0:00}:{1:00}", seconds, milliseconds);
    }

    private void HideTimer()
    {
        if (_hideFeedbacks)
        {
            _hideFeedbacks.PlayFeedbacks();
        }
    }

    private void ShowTimer()
    {
        _hasStarted = true;
        _isFinished = false;
        _secondsRemaining = numberOfSeconds;
        if (_showFeedbacks)
        {
            _showFeedbacks.PlayFeedbacks();
        }
    }

    public void DebugShowTimer()
    {
        ShowTimer();
    }

    private void CheckForLevelSettings()
    {
        LevelSettings levelSettings = LevelSettings.Instance;
        if (levelSettings)
        {
            numberOfSeconds = levelSettings.heroTimer;
            if (levelSettings.startTimer)
            {
                ShowTimer();
            }
        }
    }

    private void OnEnable()
    {
        LevelSettings.OnStartTimer += ShowTimer;
        ScenesManager.OnAllScenesLoaded += CheckForLevelSettings;
        HeroEvents.OnHeroEntered += HideTimer;
    }

    private void OnDisable()
    {
        LevelSettings.OnStartTimer -= ShowTimer;
        ScenesManager.OnAllScenesLoaded -= CheckForLevelSettings;
        HeroEvents.OnHeroEntered -= HideTimer;
    }
}
