using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

public class SkipTimeline : MonoBehaviour
{
    [SerializeField]
    private GameObject _timeline;

    public void SkipTo(int time)
    {
        PlayableDirector director = _timeline.GetComponent<PlayableDirector>();
        director.time = time;
    }
}
