using System;
using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;

public class BubbleTrigger : MonoBehaviour
{
    private Animator _anim;
    private bool _dialogueShown;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    private void Start()
    {
        StartCoroutine(ShowBubble());
    }

    IEnumerator ShowBubble()
    {
        yield return DoShowBubble(5);
        while (!_dialogueShown)
        {
            yield return DoShowBubble(10);
        }
    }

    IEnumerator DoShowBubble(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        if (!_dialogueShown)
        {
            _anim.SetTrigger("Show");
        }
    }

    private void DialogueShown()
    {
        StopAllCoroutines();
        _dialogueShown = true;
    }

    private void OnEnable()
    {
        DialogueTrigger.OnStartDialogue += DialogueShown;
    }

    private void OnDisable()
    {
        DialogueTrigger.OnStartDialogue -= DialogueShown;
    }
}
