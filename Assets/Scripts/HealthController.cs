using System.Collections;
using System.Collections.Generic;
using Events;
using MoreMountains.Feedbacks;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
   [SerializeField]
   private Image[] _hearts;

   [SerializeField]
   private MMFeedbacks _hideFeedbacks;
   [SerializeField]
   private MMFeedbacks _showFeedbacks;
   [SerializeField]
   private Transform _instructionsTransform;

   private float _fullHealth = 12;
   private float _previousHealth;
   private float _currentHealth;

   private void TakeDamage(float damage)
   {
      _currentHealth = _currentHealth - damage;

      if (_currentHealth < 1)
      {
         HeroEvents.HeroDies();
      }

      UpdateDisplay();
   }

   private void HideHealth(bool showInstructions = false)
   {
      if (_hideFeedbacks)
      {
         _hideFeedbacks.PlayFeedbacks();
      }

      if (showInstructions)
      {
         _instructionsTransform.localScale = Vector3.one;
      }
   }

   private void ShowHealth()
   {
      if (_showFeedbacks)
      {
         InitializeHealth();
         _showFeedbacks.PlayFeedbacks();
      }
   }

   private void UpdateDisplay()
   {
      StopAllCoroutines();
      StartCoroutine(UpdateHearts());
      _previousHealth = _currentHealth;
   }

   private IEnumerator UpdateHearts()
   {
      float remainingHealth = _currentHealth;
      List<Image> hearts = new List<Image>();
      foreach (var heart in _hearts)
      {
         hearts.Add(heart);
      }

      // @todo Get rid of this MESS and code smarter...
      if (_currentHealth < _previousHealth)
      {
         hearts.Reverse();

         float fill;
         Image heart = hearts[0];
         if (remainingHealth > 8)
         {
            fill = remainingHealth - 8;
            remainingHealth = remainingHealth - fill;
            yield return UpdateHeart(heart, fill);
         }
         else
         {
            fill = 0;
            yield return UpdateHeart(heart, fill);
         }

         heart = hearts[1];
         if (remainingHealth > 4)
         {
            fill = remainingHealth - 4;
            remainingHealth = remainingHealth - fill;
            yield return UpdateHeart(heart, fill);
         }
         else
         {
            yield return UpdateHeart(heart, 0);
         }

         heart = hearts[2];
         yield return UpdateHeart(heart, remainingHealth);
      }
      else
      {
         foreach (var heart in hearts)
         {
            float fill;
            if (remainingHealth > 4)
            {
               fill = 4;
            }
            else
            {
               fill = remainingHealth;
            }

            yield return UpdateHeart(heart, fill);
            remainingHealth = Mathf.Max(remainingHealth - 4, 0);
         }
      }
   }

   private IEnumerator UpdateHeart(Image heart, float targetHealth)
   {
      float targetFill = targetHealth / 4;
      while (heart.fillAmount != targetFill)
      {
         float nextFill = heart.fillAmount > targetFill ? Mathf.Clamp(heart.fillAmount - 0.25f, 0, 1) : Mathf.Clamp(heart.fillAmount + 0.25f, 0, 1);
         heart.fillAmount = nextFill;
         yield return new WaitForSecondsRealtime(0.2f);
      }
   }

   private void InitializeHealth()
   {
      _previousHealth = 0;
      _currentHealth = _fullHealth;
      UpdateDisplay();
   }

   public void TakeRandomDamage()
   {
      _currentHealth = Random.Range(0, 11);
      UpdateDisplay();
   }

   private void ResetScene()
   {
      HideHealth(true);
   }

   private void OnEnable()
   {
      ScenesManager.OnAllScenesLoaded += ResetScene;
      HeroEvents.OnHeroEntered += ShowHealth;
      HeroEvents.OnHeroTakesDamage += TakeDamage;
   }

   private void OnDisable()
   {
      ScenesManager.OnAllScenesLoaded -= ResetScene;
      HeroEvents.OnHeroEntered -= ShowHealth;
      HeroEvents.OnHeroTakesDamage -= TakeDamage;
   }
}
