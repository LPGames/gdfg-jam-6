using System;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Allows a "cold start" in the editor, when pressing Play and not passing from a scene with the ScenesManager.
/// </summary>
public class EditorColdStartup : MonoBehaviour
{
    public static event Action OnColdStartFinished;

#if UNITY_EDITOR

    private bool isColdStart;

    private AsyncOperation _loadingOperation;
    private bool _sceneIsLoading;

    private void Awake()
    {
        if (!ScenesManager.Instance)
        {
            isColdStart = true;
        }
    }

    private void Start()
    {
        if (isColdStart)
        {
            Debug.Log("is cold start");
            _loadingOperation = SceneManager.LoadSceneAsync("ColdStart", LoadSceneMode.Additive);
            _sceneIsLoading = true;
        }
        else
        {
            Debug.Log("is not cold start");
        }
    }

    void Update()
    {
        if (!_sceneIsLoading)
        {
            return;
        }

        if (_loadingOperation != null && _loadingOperation.isDone)
        {
            _sceneIsLoading = false;
            OnColdStartFinished?.Invoke();
        }
    }

#endif
}